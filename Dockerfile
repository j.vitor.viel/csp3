FROM ubuntu:18.04

ENV DEBIAN_FRONTEND="noninteractive" \
  TZ="America/Sao_Paulo" \
  JAVA_MEMORY_MIN="512m" \
  JAVA_MEMORY_MAX="2048m"

RUN apt-get update && apt-get install -y tzdata jq libxml2-utils software-properties-common iputils-ping && \
  #
  add-apt-repository -y ppa:ts.sch.gr/ppa && \
  echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
  echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections && \
  apt-get update && apt-get install -y oracle-java8-installer oracle-java8-set-default && \
  #
  echo $TZ > /etc/timezone && rm /etc/localtime && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
  dpkg-reconfigure -f noninteractive tzdata  && \
  #
  apt-get autoremove -y software-properties-common && \
  touch /var/log/cron.log && \
  apt-get clean && rm -rf /var/lib/apt/lists/*
ADD cardservproxy.tar.gz /usr/local/
COPY limits.conf /etc/security/
COPY CCcam.channelinfoClaro /usr/local/cardservproxy/etc/
COPY CCcam.channelinfoSky /usr/local/cardservproxy/etc/
COPY proxy.xml /usr/local/cardservproxy/config/



COPY ./entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
